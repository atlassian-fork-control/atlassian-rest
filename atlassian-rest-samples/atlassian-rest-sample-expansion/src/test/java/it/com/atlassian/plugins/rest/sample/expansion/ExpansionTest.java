package it.com.atlassian.plugins.rest.sample.expansion;

import com.atlassian.plugins.rest.sample.expansion.UriBuilder;
import com.atlassian.plugins.rest.sample.expansion.entity.Game;
import com.atlassian.plugins.rest.sample.expansion.entity.Player;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

public class ExpansionTest {
    public static final int POINTS_SCORED = 513;
    private UriBuilder builder;
    private Client client;

    @Before
    public void setUp() {
        client = Client.create();
        client.addFilter(new HTTPBasicAuthFilter("admin", "admin"));
        builder = UriBuilder.create().path("rest").path("ex").path("1");
    }

    @Test
    public void testGetGameWithNonExpandedPlayers() {
        final Game game = client.resource(builder.build()).path("game").get(Game.class);
        assertEquals("Rugby", game.getName());
        assertNull(game.getPlayers().getPlayers());
        assertEquals(5, game.getPlayers().getMaxResults());
        assertEquals(10, game.getPlayers().getSize());
        assertNull(game.getPlayers().getStartIndex());
    }

    @Test
    public void testGetGameWithExpandedPlayers() {
        final Game game = client.resource(builder.build()).path("game").queryParam("expand", "players").get(Game.class);
        assertEquals("Rugby", game.getName());
        assertEquals(5, game.getPlayers().getPlayers().size());
        assertEquals(5, game.getPlayers().getMaxResults());
        assertEquals(10, game.getPlayers().getSize());
        assertEquals(0, game.getPlayers().getStartIndex().intValue());

        assertEquals(0, game.getPlayers().getPlayers().get(0).getId());
        assertEquals(4, game.getPlayers().getPlayers().get(4).getId());
    }

    @Test
    public void testGetGameWithExpandedPlayersAndOffset() {
        final Game game = client.resource(builder.build()).path("game").queryParam("expand", "players[3:]").get(Game.class);
        assertEquals("Rugby", game.getName());
        assertEquals(5, game.getPlayers().getPlayers().size());
        assertEquals(5, game.getPlayers().getMaxResults());
        assertEquals(10, game.getPlayers().getSize());
        assertEquals(3, game.getPlayers().getStartIndex().intValue());

        assertEquals(3, game.getPlayers().getPlayers().get(0).getId());
        assertEquals(7, game.getPlayers().getPlayers().get(4).getId());
    }

    @Test
    public void testGetGameWithExpandedPlayersAndOffsetOutOfBounds() {
        final Game game = client.resource(builder.build()).path("game").queryParam("expand", "players[12:]").get(Game.class);
        assertEquals("Rugby", game.getName());
        assertNull(game.getPlayers().getPlayers());
        assertEquals(5, game.getPlayers().getMaxResults());
        assertEquals(10, game.getPlayers().getSize());
        assertNull(game.getPlayers().getStartIndex());
    }

    @Test
    public void testGetPlayerNames() {
        final Player player1 = client.resource(builder.build()).path("player").path("0").get(Player.class);
        final Player player2 = client.resource(builder.build()).path("player").path("1").get(Player.class);
        assertEquals("Adam Ashley-Cooper", player1.getFullName());
        assertEquals("Matt Giteau", player2.getFullName());
    }

    @Test
    public void testGetPlayerWithExpandedMultipleSubItemsSamePrefix() {
        WebResource resource = client.resource(builder.param("expand", "record.subRecord1,record.subRecord2").build()).path("player").path("2");
        final Player player = resource.get(Player.class);
        assertThat(player.getRecord().getSubRecord1(), notNullValue());
        assertThat(player.getRecord().getSubRecord2(), notNullValue());

        assertThat(player.getRecord().getSubRecord1().getPointsScored(), is(POINTS_SCORED / 2));
        assertThat(player.getRecord().getSubRecord2().getPointsScored(), is(POINTS_SCORED / 2));
    }

    @Test
    public void testPlayerWithRecordCanBeExpanded() {
        // get without expansion
        Player player = client.resource(builder.build()).path("player").path("2").get(Player.class);
        assertNotNull(player.getRecord());
        // now expand it
        player = client.resource(builder.param("expand", "record").build()).path("player").path("2").get(Player.class);
        assertNotNull(player.getRecord());
        assertEquals((Integer) POINTS_SCORED, player.getRecord().getPointsScored());
    }

    @Test
    public void testPlayerWithNoRecordCanNotBeExpanded() {
        // get without expansion
        Player player = client.resource(builder.build()).path("player").path("1").get(Player.class);
        assertNull(player.getRecord());
        // now expand it
        player = client.resource(builder.param("expand", "record").build()).path("player").path("1").get(Player.class);
        assertNull(player.getRecord());
    }

}
