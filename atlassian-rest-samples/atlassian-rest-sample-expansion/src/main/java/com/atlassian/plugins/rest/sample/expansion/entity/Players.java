package com.atlassian.plugins.rest.sample.expansion.entity;

import com.atlassian.plugins.rest.common.expand.entity.AbstractPagedListWrapper;
import com.atlassian.plugins.rest.common.expand.entity.ListWrapperCallBacks;
import com.atlassian.plugins.rest.common.expand.entity.ListWrapperCallback;
import com.atlassian.plugins.rest.sample.expansion.resource.DataStore;

import javax.ws.rs.core.UriBuilder;

import static javax.xml.bind.annotation.XmlAccessType.FIELD;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.List;

@XmlRootElement
@XmlAccessorType(FIELD)
public class Players extends AbstractPagedListWrapper<Player> {
    @XmlElement(name = "player")
    private List<Player> players;

    @XmlTransient
    private UriBuilder uriBuilder;

    // for JAXB
    private Players() {
        super(0, 0);
    }

    public Players(int size, int maxItems, UriBuilder uriBuilder) {
        super(size, maxItems);
        this.uriBuilder = uriBuilder;
    }

    public ListWrapperCallback<Player> getPagingCallback() {
        return ListWrapperCallBacks.ofList(DataStore.getInstance().getPlayers(uriBuilder), getMaxResults());
    }

    public List<Player> getPlayers() {
        return players;
    }
}
