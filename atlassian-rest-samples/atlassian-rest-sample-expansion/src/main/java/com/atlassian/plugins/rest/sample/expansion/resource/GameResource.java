package com.atlassian.plugins.rest.sample.expansion.resource;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import static com.atlassian.plugins.rest.sample.expansion.resource.PlayerResource.getPlayerUriBuilder;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.APPLICATION_XML;

/**
 * Resource representing a Game such as Rugby, Football (and I mean soccer), etc.
 */
@Path("/game")
@Consumes({APPLICATION_XML, APPLICATION_JSON})
@Produces({APPLICATION_XML, APPLICATION_JSON})
public class GameResource {
    @Context
    private UriInfo uriInfo;

    @GET
    public Response getGame() {
        return Response.ok(DataStore.getInstance().getGame("rugby", getPlayerUriBuilder(uriInfo))).build();
    }
}
