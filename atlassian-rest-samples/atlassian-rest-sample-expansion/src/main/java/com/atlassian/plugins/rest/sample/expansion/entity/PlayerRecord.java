package com.atlassian.plugins.rest.sample.expansion.entity;

import com.atlassian.plugins.rest.common.expand.Expandable;
import com.atlassian.plugins.rest.common.expand.Expander;

import static javax.xml.bind.annotation.XmlAccessType.FIELD;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * The total points scoring record for a player.
 */
@XmlRootElement
@XmlAccessorType(FIELD)
@Expander(PlayerRecordExpander.class)
public class PlayerRecord {
    /**
     * Creates an empty record which is used to inform the client that a record is available without
     * performing the cost of calculating it.  If the client requires the record it can expand this field.
     *
     * @param player
     * @return
     */
    public static PlayerRecord emptyRecord(Player player) {
        return new PlayerRecord(player);
    }

    @XmlAttribute
    private String expand;

    @XmlElement
    private Integer pointsScored;

    @XmlTransient
    private Player player;

    @Expandable
    @XmlElement
    private SubRecord subRecord1;

    @Expandable
    @XmlElement
    private SubRecord subRecord2;

    public PlayerRecord() {
    }

    public PlayerRecord(Player player) {
        this.player = player;
    }

    public PlayerRecord(int pointsScored) {
        this.pointsScored = pointsScored;
    }

    public Integer getPointsScored() {
        return pointsScored;
    }

    public void setPointsScored(Integer pointsScored) {
        this.pointsScored = pointsScored;
    }

    public void setSubRecord1(final SubRecord subRecord1) {
        this.subRecord1 = subRecord1;
    }

    public SubRecord getSubRecord1() {
        return subRecord1;
    }

    public void setSubRecord2(final SubRecord subRecord2) {
        this.subRecord2 = subRecord2;
    }

    public SubRecord getSubRecord2() {
        return subRecord2;
    }

    public Player getPlayer() {
        return player;
    }
}