package com.atlassian.plugins.rest.sample.helloworld;

import org.junit.Test;

import static org.junit.Assert.*;

public class HelloWorldTest {
    private HelloWorld helloWorld = new HelloWorld();

    @Test
    public void testSayHelloTo() {
        assertEquals("Hello world", helloWorld.sayHelloTo("world"));
    }
}
