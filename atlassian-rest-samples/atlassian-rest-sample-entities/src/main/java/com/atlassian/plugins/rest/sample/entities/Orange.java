package com.atlassian.plugins.rest.sample.entities;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
public class Orange {
    String name;

    public Orange() {
    }

    public Orange(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
