package it.com.atlassian.plugins.rest.sample.entities;

import com.atlassian.plugins.rest.sample.entities.EntityClientServlet;
import com.atlassian.plugins.rest.sample.entities.EntityResource;
import com.atlassian.plugins.rest.sample.entities.UriBuilder;
import com.sun.jersey.api.client.Client;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.io.IOUtils;
import org.hamcrest.CoreMatchers;
import org.hamcrest.Matchers;
import org.junit.Test;

import javax.ws.rs.core.MediaType;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;

import static com.atlassian.plugins.rest.sample.entities.EntityClientServlet.P_ACCEPT;
import static com.atlassian.plugins.rest.sample.entities.EntityClientServlet.P_CONTENT_TYPE;
import static com.atlassian.plugins.rest.sample.entities.EntityClientServlet.P_ORANGE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

/**
 * Testing {@link EntityResource}
 */
public class EntityTest {
    /* A secret that shouldn't be exposed to anonymous requests */
    private static final String SECRET = Long.toString(Double.doubleToLongBits(Math.random()));

    protected File createSecretFile() throws IOException {
        final Path path = Files.createTempFile("secret-file", ".txt");
        Files.write(path, SECRET.getBytes());
        File f = path.toFile();
        f.deleteOnExit();
        return f;
    }

    /**
     * Simple test that executes {@link EntityClientServlet}, which uses the {@link com.atlassian.sal.api.net.RequestFactory} to query
     * {@link EntityResource}
     */
    @Test
    public void testApplesForOranges() {
        final URI baseUri = UriBuilder.create().path("plugins").path("servlet").path("client").build();
        final String returned = Client.create().resource(baseUri).queryParam(P_ORANGE, "valencia").get(String.class);
        assertEquals(
                "apple-valencia\n" +
                        "Content-Type=application/xml\n" +
                        "Accept=application/xml", returned);   // application/xml is the default content-type & accept header value
    }

    @Test
    public void testApplesForOrangesJson() {
        final URI baseUri = UriBuilder.create().path("plugins").path("servlet").path("client").build();
        final String returned = Client.create().resource(baseUri)
                .queryParam(P_ORANGE, "delfino")
                .queryParam(P_CONTENT_TYPE, "application/json")
                .queryParam(P_ACCEPT, "application/json")
                .get(String.class);
        assertEquals(
                "apple-delfino\n" +
                        "Content-Type=application/json\n" +
                        "Accept=application/json", returned);
    }

    @Test
    public void testApplesForOrangesDefaultContentTypeAcceptJson() {
        final URI baseUri = UriBuilder.create().path("plugins").path("servlet").path("client").build();
        final String returned = Client.create().resource(baseUri)
                .queryParam(P_ORANGE, "delfino")
                .queryParam(P_ACCEPT, "application/json")
                .get(String.class);
        assertEquals(
                "apple-delfino\n" +
                        "Content-Type=application/xml\n" +
                        "Accept=application/json", returned);
    }

    @Test
    public void testApplesForOrangesExplicitXmlContentTypeAndAccept() {
        final URI baseUri = UriBuilder.create().path("plugins").path("servlet").path("client").build();
        final String returned = Client.create().resource(baseUri)
                .queryParam(P_ORANGE, "delfino")
                .queryParam(P_CONTENT_TYPE, "application/xml")
                .queryParam(P_ACCEPT, "application/xml")
                .get(String.class);
        assertEquals(
                "apple-delfino\n" +
                        "Content-Type=application/xml\n" +
                        "Accept=application/xml", returned);
    }

    @Test
    public void testApplesForOrangesMissingAcceptHeaderDefaultsToContentType() {
        final URI baseUri = UriBuilder.create().path("plugins").path("servlet").path("client").build();
        final String returned = Client.create().resource(baseUri)
                .queryParam(P_ORANGE, "delfino")
                .queryParam(P_CONTENT_TYPE, "application/json")
                .get(String.class);
        assertEquals(
                "apple-delfino\n" +
                        "Content-Type=application/json\n" +
                        "Accept=application/json", returned);
    }

    @Test
    public void testApplesForOrangesMultipleValidAcceptHeaders() {
        final URI baseUri = UriBuilder.create().path("plugins").path("servlet").path("client").build();
        final String returned = Client.create().resource(baseUri)
                .queryParam(P_ORANGE, "delfino")
                .queryParam(P_ACCEPT, "application/json")
                .queryParam(P_ACCEPT, "application/xml")
                .get(String.class);
        assertEquals(
                "apple-delfino\n" +
                        "Content-Type=application/xml\n" + //default
                        "Accept=application/json,application/xml", returned);
    }


    private PostMethod postToRestEndpoint(String content) throws IOException {
        RequestEntity re = new StringRequestEntity(content, "application/xml", "us-ascii");

        final URI baseUri = UriBuilder.create().path("rest").path("entity").path("latest").path("fruit").build();

        HttpClient client = new HttpClient();

        PostMethod m = new PostMethod(baseUri.toString());

        m.setRequestHeader("Accept", "application/xml");
        m.setRequestEntity(re);

        client.executeMethod(m);

        return m;
    }

    @Test
    public void testEntityExpansionDoesNotIncludeFileContents() throws IOException {
        try (InputStream in = ClassLoader.getSystemResourceAsStream("EntityTest-rest-include-external-entity.xml")) {
            assertNotNull(in);

            String contents = new String(IOUtils.toByteArray(in), "us-ascii");

            contents = contents.replace("/etc/passwd", createSecretFile().toURI().toString());

            PostMethod m = postToRestEndpoint(contents);

            String resp = new String(IOUtils.toByteArray(m.getResponseBodyAsStream()), "us-ascii");

            MediaType mt = MediaType.valueOf(m.getResponseHeader("content-type").getValue());

            assertEquals("The response should be XML",
                    "application/xml",
                    mt.getType() + '/' + mt.getSubtype()
            );
            assertThat(resp, CoreMatchers.not(CoreMatchers.containsString(SECRET)));
        }
    }

    @Test
    public void testValidEntitiesAreExpanded() throws IOException {
        try (InputStream in = ClassLoader.getSystemResourceAsStream("EntityTest-rest-with-amp-entity.xml")) {
            assertNotNull(in);

            String contents = new String(IOUtils.toByteArray(in), "us-ascii");

            PostMethod m = postToRestEndpoint(contents);

            String resp = new String(IOUtils.toByteArray(m.getResponseBodyAsStream()), "us-ascii");

            MediaType mt = MediaType.valueOf(m.getResponseHeader("content-type").getValue());

            assertEquals("The response should be XML",
                    "application/xml",
                    mt.getType() + '/' + mt.getSubtype()
            );
            assertThat(resp, CoreMatchers.containsString("apple-&amp;"));
        }
    }

    @Test
    public void testEntityExpansionDoesNotCauseDenialOfService() throws IOException {
        try (InputStream in = ClassLoader.getSystemResourceAsStream("EntityTest-rest-billion-laughs.xml")) {
            assertNotNull(in);

            String contents = new String(IOUtils.toByteArray(in), "us-ascii");

            PostMethod m = postToRestEndpoint(contents);

            String resp = new String(IOUtils.toByteArray(m.getResponseBodyAsStream()), "us-ascii");

            // For this deployment this means a simple syntax error, in some environments you will actually get a OOME if this fails.
            // REST doesn't send any content with UnmarshalException, so any content arrives is a status code template
            // of the web server used which is not stable. The only reliable indicator of behavior is the status code
            // which should indicate a bad request rather than a server error
            assertThat("The response should not indicate a server error",
                    m.getStatusCode(), Matchers.is(400));

            // TODO [pandronov]: That is completely useless check. EOM like any other JVM Error can't be
            // catched in any reliable way and JVM behavior doens't define after an Error. In other words
            // there is no any guarantee to get anything that makes sense if it was an Error on server
            // side....
            assertThat("The response should not indicate a server memory error",
                    resp, CoreMatchers.not(CoreMatchers.containsString("java.lang.OutOfMemoryError")));
        }
    }

    @Test
    public void fruitRetrievedAsXmlObeysJaxbAnnotations() {
        final URI baseUri = UriBuilder.create().path("rest/entity/1/fruit/jackfruit").build();
        final String returned = Client.create().resource(baseUri)
                .accept(MediaType.APPLICATION_XML)
                .get(String.class);

        assertThat(returned, Matchers.containsString("<jackFruit jaxb-description=\""));
    }

    @Test
    public void fruitRetrievedAsJsonObeysJacksonAnnotations() {
        final URI baseUri = UriBuilder.create().path("rest/entity/1/fruit/jackfruit").build();
        final String returned = Client.create().resource(baseUri)
                .accept(MediaType.APPLICATION_JSON)
                .get(String.class);

        assertThat(returned, Matchers.startsWith("{\"json-description\":\"fresh at"));
    }
}
