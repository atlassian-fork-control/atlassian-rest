package it.com.atlassian.plugins.rest.sample.entities;

import com.atlassian.plugins.rest.sample.entities.UriBuilder;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

import javax.ws.rs.core.MediaType;
import java.net.URI;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * Testing a {@link com.atlassian.plugins.rest.sample.entities.EntityResource} that has no versions.
 */
public class NoVersionTest {
    @Test
    public void testResourceWithNoVersion() {
        // URI does not contain a version number:
        // the "/entity-versionless" path in atlassian-plugin.xml specifies version="none",
        // which causes the RestModuleDescriptor to generate a url-pattern using a path generated by
        // a RestApiContext created with a parameter ApiVersion("none"), which does not put a version in the path
        final URI baseUri = UriBuilder.create().path("rest/entity-versionless/fruit/jackfruit").build();
        final String returned = Client.create().resource(baseUri)
                .accept(MediaType.APPLICATION_XML)
                .get(String.class);

        Assert.assertThat(returned, Matchers.containsString("<jackFruit jaxb-description=\"fresh at"));
    }

    @Test
    public void testLatestVersionOfResourceWithNoVersion() {
        // *everything* is the latest version, so this still works
        final URI baseUri = UriBuilder.create().path("rest/entity-versionless/latest/fruit/jackfruit").build();
        final String returned = Client.create().resource(baseUri)
                .accept(MediaType.APPLICATION_XML)
                .get(String.class);

        Assert.assertThat(returned, Matchers.containsString("<jackFruit jaxb-description=\"fresh at"));
    }

    @Test
    public void testSpecificVersionOfResourceWithNoVersion() {
        // there are no named versions so this cannot possibly work
        final URI baseUri = UriBuilder.create().path("rest/entity-versionless/1/fruit/jackfruit").build();
        ClientResponse response = Client.create().resource(baseUri).get(ClientResponse.class);
        assertThat(response.getStatusInfo().getStatusCode(), is(ClientResponse.Status.NOT_FOUND.getStatusCode()));
    }
}
