# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [6.0.0 - Unreleased]

### Changed
* [REST-421]: Updated dependency versions to make this library Platform 5 and Java 11 compatible

## [3.5.0 - Unreleased]

### Added
* [REST-420]: Updates for compatibility with new plugin framework ServletModuleManager methods for working with
DispatcherType enums.


## [3.2.18] - 2018-05-22

### Fixed
* [REST-417]: fixed urls containing IPv6 IP addresses not being handled correctly

[3.2.18]: https://bitbucket.org/atlassian/atlassian-rest/branches/compare/atlassian-rest-parent-3.2.18%0Datlassian-rest-parent-3.2.17
[REST-417]: https://ecosystem.atlassian.net/browse/REST-417
