package com.atlassian.plugins.rest.validation;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import com.atlassian.plugins.rest.common.interceptor.InterceptorChain;
import com.atlassian.plugins.rest.common.transaction.TransactionInterceptor;
import com.atlassian.plugins.rest.common.validation.ValidationInterceptor;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.APPLICATION_XML;

@Path("/validatedResource")
@InterceptorChain({ValidationInterceptor.class, TransactionInterceptor.class})
public class ValidatedResource {
    @POST
    @Consumes({APPLICATION_XML, APPLICATION_JSON})
    @Produces({APPLICATION_XML, APPLICATION_JSON})
    public PersonResponse addPerson(Person person) {
        PersonResponse res = new PersonResponse();
        res.setName(person.getName());
        return res;
    }
}