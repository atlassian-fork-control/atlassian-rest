package com.atlassian.plugins.rest.helloworld;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.plugins.rest.common.security.AuthenticationContext;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.RequestFactory;
import com.atlassian.sal.api.net.ResponseException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;

import java.security.Principal;

@Path("/helloworld")
public class HelloWorld {
    private final RequestFactory requestFactory;

    public HelloWorld(RequestFactory requestFactory) {
        this.requestFactory = requestFactory;
    }

    @GET
    @Produces("text/plain")
    @Path("/authenticated")
    public String getAuthenticatedMessage() {
        return "Hello Authenticated World";
    }

    @GET
    @Produces("text/plain")
    @Path("/callingself")
    @AnonymousAllowed
    public String getAnonymousMessageFromSelf() throws ResponseException {
        String baseUrl = System.getProperty("baseurl");
        Request request = requestFactory.createRequest(Request.MethodType.GET, baseUrl + "/rest/refimpl/1/helloworld/anonymous");
        return request.execute();
    }

    @GET
    @Produces("text/plain")
    @Path("/anonymous")
    @AnonymousAllowed
    public String getAnonymousMessage() {
        return "Hello Anonymous World";
    }

    @GET
    @Produces("text/plain")
    @Path("/locale")
    @AnonymousAllowed
    public String getPlolyglotMessage(@Context HttpHeaders headers) {
        return "Request locales: " + headers.getAcceptableLanguages();
    }

    @GET
    @Produces("text/plain")
    @Path("/admin")
    @AnonymousAllowed
    public String getMessageForAdmin(@Context AuthenticationContext authenticationContext) {
        checkIsUser(authenticationContext, "admin");
        return "Hello " + authenticationContext.getPrincipal();
    }

    private void checkIsUser(AuthenticationContext context, String userName) {
        final Principal principal = context.getPrincipal();
        if (principal == null || !principal.getName().equals(userName)) {
            throw new SecurityException("You're not '" + userName + "' I know who you really are'" + principal + "', you can't access this information");
        }
    }
}
