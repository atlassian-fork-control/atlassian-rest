package com.atlassian.plugins.rest.test;

import com.atlassian.plugins.rest.common.expand.Expandable;
import com.atlassian.plugins.rest.common.expand.entity.ListWrapper;
import com.atlassian.plugins.rest.common.expand.entity.ListWrapperCallback;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.List;

import static java.util.Objects.requireNonNull;
import static javax.xml.bind.annotation.XmlAccessType.FIELD;

@XmlRootElement
@XmlAccessorType(FIELD)
public class Developers implements ListWrapper<Developer> {
    @XmlAttribute
    private String expand;

    @XmlAttribute
    private int size;

    @XmlElement(name = "developer")
    @Expandable
    private List<Developer> developers;

    @XmlTransient
    private final ListWrapperCallback<Developer> callback;

    @SuppressWarnings("unused")
    private Developers() {
        this.size = 0;
        callback = null;
    }

    public Developers(int size, ListWrapperCallback<Developer> callback) {
        this.size = size;
        this.callback = requireNonNull(callback);
    }

    public int getSize() {
        return size;
    }

    public void setDevelopers(List<Developer> developers) {
        this.developers = developers;
    }

    public List<Developer> getDevelopers() {
        return developers;
    }

    public ListWrapperCallback<Developer> getCallback() {
        return callback;
    }
}
