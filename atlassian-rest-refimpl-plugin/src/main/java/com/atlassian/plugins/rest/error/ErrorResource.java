package com.atlassian.plugins.rest.error;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

@Path("/errors")
@AnonymousAllowed
@Produces({"application/xml", "application/json", "text/plain"})
public class ErrorResource {
    @GET
    @Path("/uncaughtInternalError")
    public Object get(@QueryParam("message") String message) {
        throw new IllegalStateException(message);
    }
}
