package com.atlassian.plugins.rest.test;

import static javax.xml.bind.annotation.XmlAccessType.*;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@XmlRootElement
@XmlAccessorType(PROPERTY)
public class Projects {
    private final Map<String, Project> projects;

    public Projects() {
        projects = new HashMap<String, Project>() {{
            final Project project1 = new Project("project1", "Description of project 1");
            final Project project2 = new Project("project2", "Description of project 2");

            put(project1.getName(), project1);
            put(project2.getName(), project2);
        }};
    }

    public Map<String, Project> getProjectsMap() {
        return Collections.unmodifiableMap(projects);
    }

    @XmlElement
    public Collection<Project> getProjects() {
        return Collections.unmodifiableCollection(projects.values());
    }
}
