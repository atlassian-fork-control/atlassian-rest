package com.atlassian.plugins.rest.cors;

import com.atlassian.plugins.rest.common.security.descriptor.CorsDefaults;

import java.util.Arrays;
import java.util.Collection;
import java.util.Set;

import static java.util.Collections.singleton;
import static java.util.Collections.unmodifiableList;

/**
 * A basic implementation of {@link CorsDefaults} purely for testing purposes. This is not published by the REST module.
 */
public class SimpleCorsDefaults implements CorsDefaults {
    public static final String CREDENTIALS = "http://credentials.test.com";
    public static final String NO_CREDENTIALS = "http://nocredentials.test.com";

    private static final Collection<String> WHITELIST = unmodifiableList(Arrays.asList(CREDENTIALS, NO_CREDENTIALS));

    public boolean allowsCredentials(String uri) throws IllegalArgumentException {
        return CREDENTIALS.equals(uri);
    }

    public boolean allowsOrigin(String uri) {
        return WHITELIST.contains(uri);
    }

    public Set<String> getAllowedRequestHeaders(String uri) {
        return singleton("X-Custom-Header");
    }

    public Set<String> getAllowedResponseHeaders(String uri) {
        return singleton("X-Response-Header");
    }
}
