package com.atlassian.plugins.rest.test;

import com.atlassian.plugins.rest.common.expand.AbstractRecursiveEntityExpander;

public class ProjectInformationExpander extends AbstractRecursiveEntityExpander<ProjectInformation> {
    protected ProjectInformation expandInternal(ProjectInformation information) {
        if (information.getLink().getHref().toString().contains("project1")) {
            information.setLongName("Project One");
            information.setLongDescription("This is project One. It comes before project Two.");
        }
        return information;
    }
}
