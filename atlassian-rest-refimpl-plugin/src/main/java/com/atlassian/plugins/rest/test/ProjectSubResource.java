package com.atlassian.plugins.rest.test;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import static java.util.Objects.requireNonNull;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.APPLICATION_XML;

@Consumes({APPLICATION_XML, APPLICATION_JSON})
@Produces({APPLICATION_XML, APPLICATION_JSON})
public class ProjectSubResource {
    private final Project project;
    private final UriInfo uriInfo;

    public ProjectSubResource(Projects projects, String projectName, UriInfo uriInfo) {
        this.project = projects.getProjectsMap().get(projectName);
        this.uriInfo = requireNonNull(uriInfo);
    }

    @GET
    public Response getProject() {
        return project != null ? Response.ok(project.build(uriInfo)).build() : Response.status(Response.Status.NOT_FOUND).build();
    }
}