package com.atlassian.plugins.rest.test;

import com.atlassian.plugins.rest.common.expand.AbstractRecursiveEntityExpander;

public class FavouriteDrinkExpander extends AbstractRecursiveEntityExpander<FavouriteDrink> {
    protected FavouriteDrink expandInternal(FavouriteDrink drink) {
        if (drink.getName().equals("coffee")) {
            drink.setDescription("Coffee is a brewed beverage prepared from roasted seeds, commonly called coffee beans, of the coffee plant." +
                    " Due to its caffeine content, coffee has a stimulating effect in humans. Today, coffee is one of the most popular beverages worldwide (wikipedia)");
        }
        return drink;
    }
}
