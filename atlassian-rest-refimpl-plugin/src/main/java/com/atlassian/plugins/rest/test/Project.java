package com.atlassian.plugins.rest.test;

import com.atlassian.plugins.rest.common.expand.Expandable;
import com.atlassian.plugins.rest.common.expand.entity.ListWrapperCallBacks;
import com.google.common.collect.ImmutableList;

import javax.ws.rs.core.UriInfo;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collections;
import java.util.List;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Project {
    @XmlAttribute
    private String expand;

    @XmlAttribute
    private final String name;

    @XmlElement
    private final String description;

    @XmlElement
    @Expandable
    private ProjectInformation information;

    @XmlElement
    @Expandable
    private Developers developers;

    // For JAXB
    private Project() {
        this.name = null;
        this.description = null;
    }

    public Project(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public ProjectInformation getInformation() {
        return information;
    }

    public void setInformation(ProjectInformation information) {
        this.information = information;
    }

    public List<Developer> getDevelopers() {
        return developers.getDevelopers() != null ? ImmutableList.copyOf(developers.getDevelopers()) : Collections.<Developer>emptyList();
    }

    public void setDevelopers(Developers developers) {
        this.developers = developers;
    }

    public Project build(UriInfo uriInfo) {
        setInformation(ProjectInformation.getInformation(uriInfo));

        final List<Developer> developerList = DeveloperStore.getDevelopers(uriInfo);
        setDevelopers(new Developers(developerList.size(), ListWrapperCallBacks.ofList(developerList)));
        return this;
    }
}
