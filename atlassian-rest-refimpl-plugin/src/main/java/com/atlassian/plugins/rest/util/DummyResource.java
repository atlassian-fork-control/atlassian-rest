package com.atlassian.plugins.rest.util;

import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("/dummy")
public class DummyResource {
    public DummyResource() {
    }

    @Path("/sub")
    public Response subResource() {
        return null;
    }
}
