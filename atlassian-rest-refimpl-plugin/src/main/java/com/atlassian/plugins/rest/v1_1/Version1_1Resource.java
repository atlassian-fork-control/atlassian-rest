package com.atlassian.plugins.rest.v1_1;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("/version1_1Resource")
public class Version1_1Resource {
    @GET
    @Produces("text/plain")
    public String getMessage() {
        return "Version 1.1 Hello World";
    }
}
