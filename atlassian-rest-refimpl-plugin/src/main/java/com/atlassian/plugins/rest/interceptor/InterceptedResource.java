package com.atlassian.plugins.rest.interceptor;

import com.atlassian.plugins.rest.common.interceptor.InterceptorChain;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.plugins.rest.common.security.AuthenticationContext;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import java.security.Principal;

@Path("/interceptedResource")
@InterceptorChain({MessageInterceptor.class})
public class InterceptedResource {
    @GET
    @Produces("text/xml")
    public Message getMessage() {
        return new Message("Hello World");
    }
}