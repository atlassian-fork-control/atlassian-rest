package com.atlassian.plugins.rest.test;

import com.atlassian.plugins.rest.common.expand.Expandable;
import com.atlassian.plugins.rest.common.expand.entity.ListWrapper;
import com.atlassian.plugins.rest.common.expand.entity.ListWrapperCallback;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.List;

import static java.util.Objects.requireNonNull;
import static javax.xml.bind.annotation.XmlAccessType.FIELD;

@XmlRootElement
@XmlAccessorType(FIELD)
public class FavouriteDrinks implements ListWrapper<FavouriteDrink> {
    @XmlAttribute
    private String expand;

    @XmlAttribute
    private int size;

    @XmlElement(name = "drink")
    @Expandable
    private List<FavouriteDrink> favouriteDrinks;

    @XmlTransient
    private final ListWrapperCallback<FavouriteDrink> callback;

    @SuppressWarnings("unused")
    private FavouriteDrinks() {
        this.size = 0;
        callback = null;
    }

    public FavouriteDrinks(int size, ListWrapperCallback<FavouriteDrink> callback) {
        this.size = size;
        this.callback = requireNonNull(callback);
    }

    public int getSize() {
        return size;
    }

    public List<FavouriteDrink> getFavouriteDrinks() {
        return favouriteDrinks;
    }

    public void setFavouriteDrinks(List<FavouriteDrink> favouriteDrinks) {
        this.favouriteDrinks = favouriteDrinks;
    }

    public ListWrapperCallback<FavouriteDrink> getCallback() {
        return callback;
    }
}
