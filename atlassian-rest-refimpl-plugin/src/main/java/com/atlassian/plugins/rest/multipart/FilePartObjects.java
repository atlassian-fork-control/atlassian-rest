package com.atlassian.plugins.rest.multipart;

import java.util.Collection;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class FilePartObjects {
    @XmlElement(name = "filePartObject")
    private Collection<FilePartObject> fileParts;

    public FilePartObjects() {
    }

    public FilePartObjects(final Collection<FilePartObject> fileParts) {
        this.fileParts = fileParts;
    }

    public Collection<FilePartObject> getFileParts() {
        return fileParts;
    }
}
