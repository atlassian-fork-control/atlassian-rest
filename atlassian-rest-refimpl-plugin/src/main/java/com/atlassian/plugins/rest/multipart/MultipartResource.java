package com.atlassian.plugins.rest.multipart;

import com.atlassian.plugins.rest.common.interceptor.InterceptorChain;
import com.atlassian.plugins.rest.common.multipart.FilePart;
import com.atlassian.plugins.rest.common.multipart.MultipartConfig;
import com.atlassian.plugins.rest.common.multipart.MultipartConfigClass;
import com.atlassian.plugins.rest.common.multipart.MultipartFormParam;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.plugins.rest.interceptor.Message;
import com.atlassian.plugins.rest.interceptor.MessageInterceptor;

import java.util.ArrayList;
import java.util.Collection;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

/**
 */
@Path("/multipart")
@AnonymousAllowed
public class MultipartResource {
    @POST
    @Path("single")
    public FilePartObject showMultipartSingle(@MultipartFormParam("file") FilePart filePart) {
        return new FilePartObject("file", filePart.isFormField(), filePart.getName(), filePart.getContentType(), filePart.getValue());
    }

    @POST
    @Path("multiple")
    public FilePartObjects showMultipartMultiple(@MultipartFormParam("file") Collection<FilePart> fileParts) {
        Collection<FilePartObject> objects = new ArrayList<FilePartObject>();
        for (FilePart filePart : fileParts) {
            objects.add(new FilePartObject("file", filePart.isFormField(), filePart.getName(), filePart.getContentType(), filePart.getValue()));
        }
        return new FilePartObjects(objects);
    }

    @POST
    @Path("config")
    @MultipartConfigClass(MultipartResource.SmallMultipartConfig.class)
    public FilePartObjects showMultipartConfig(@MultipartFormParam("file") Collection<FilePart> fileParts) {
        Collection<FilePartObject> objects = new ArrayList<FilePartObject>();
        for (FilePart filePart : fileParts) {
            objects.add(new FilePartObject("file", filePart.isFormField(), filePart.getName(), filePart.getContentType(), filePart.getValue()));
        }
        return new FilePartObjects(objects);
    }


    @POST
    @Path("fileName")
    @InterceptorChain(MessageInterceptor.class)
    public Message returnFileName(@MultipartFormParam("file") FilePart filePart) {
        return new Message(filePart.getName());
    }

    public static class SmallMultipartConfig implements MultipartConfig {
        public long getMaxFileSize() {
            return 10;
        }

        public long getMaxSize() {
            return 1000;
        }
    }

}
