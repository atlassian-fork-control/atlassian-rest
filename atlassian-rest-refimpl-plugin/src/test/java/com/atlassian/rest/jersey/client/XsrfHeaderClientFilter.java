package com.atlassian.rest.jersey.client;

import com.atlassian.plugins.rest.common.security.jersey.XsrfResourceFilter;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientRequest;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.filter.ClientFilter;

public class XsrfHeaderClientFilter extends ClientFilter {
    @Override
    public ClientResponse handle(final ClientRequest cr) throws ClientHandlerException {
        if (!cr.getHeaders().containsKey(XsrfResourceFilter.TOKEN_HEADER)) {
            cr.getHeaders().add(XsrfResourceFilter.TOKEN_HEADER, XsrfResourceFilter.NO_CHECK);
        }
        return getNext().handle(cr);
    }
}
