package it.com.atlassian.rest.validation;

import javax.ws.rs.core.MediaType;

import com.atlassian.plugins.rest.common.validation.ValidationErrors;
import com.atlassian.plugins.rest.validation.Person;
import com.atlassian.plugins.rest.validation.PersonResponse;
import com.atlassian.rest.jersey.client.WebResourceFactory;

import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

public class ValidatedResourceTest {
    private WebResource webResource;

    @Before
    public void setUp() {
        webResource = WebResourceFactory.authenticated();
    }

    @Test
    public void testValidationPassed() {
        Person goodPerson = new Person();
        goodPerson.setName("Jim");

        PersonResponse response = webResource.path("validatedResource.xml").entity(goodPerson, MediaType.APPLICATION_XML).post(PersonResponse.class);
        assertEquals("Jim", response.getName());
    }

    @Test
    public void testValidationPassedJson() {
        Person goodPerson = new Person();
        goodPerson.setName("Jim");

        PersonResponse response = webResource.path("validatedResource.json").entity(goodPerson, MediaType.APPLICATION_JSON).post(PersonResponse.class);
        assertEquals("Jim", response.getName());
    }

    @Test
    public void testValidationFailed() {
        Person badPerson = new Person();
        badPerson.setName("a");

        try {
            webResource.path("validatedResource.xml").entity(badPerson, MediaType.APPLICATION_XML).post(ValidationErrors.class);
            fail();
        } catch (UniformInterfaceException ex) {
            ValidationErrors errors = ex.getResponse().getEntity(ValidationErrors.class);
            assertNotNull(errors);
            assertEquals(1, errors.getErrors().size());
            assertEquals("The name must be between 2 and 10 characters", errors.getErrors().get(0).getMessage());
        }
    }

    @Test
    public void testValidationFailedJson() {
        Person badPerson = new Person();
        badPerson.setName("a");

        try {
            webResource.path("validatedResource.json").entity(badPerson, MediaType.APPLICATION_JSON).post(ValidationErrors.class);
            fail();
        } catch (UniformInterfaceException ex) {
            ValidationErrors errors = ex.getResponse().getEntity(ValidationErrors.class);
            assertNotNull(errors);
            assertEquals(1, errors.getErrors().size());
            assertEquals("The name must be between 2 and 10 characters", errors.getErrors().get(0).getMessage());
        }
    }
}