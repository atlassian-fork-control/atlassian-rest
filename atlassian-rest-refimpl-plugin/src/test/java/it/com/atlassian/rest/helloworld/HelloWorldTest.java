package it.com.atlassian.rest.helloworld;

import com.atlassian.rest.jersey.client.WebResourceFactory;

import static com.atlassian.rest.jersey.client.WebResourceFactory.LATEST;
import static com.atlassian.rest.jersey.client.WebResourceFactory.REST_VERSION;
import static com.atlassian.rest.jersey.client.WebResourceFactory.REST_VERSION_2;

import com.sun.jersey.api.client.UniformInterfaceException;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import static javax.ws.rs.core.Response.Status.UNAUTHORIZED;

public class HelloWorldTest {
    @Test
    public void testGetAnonymousHelloWorldWhenNotAuthenticated() {
        assertGetAnonymousHelloWorldWhenNotAuthenticated(REST_VERSION, "Hello Anonymous World");
        assertGetAnonymousHelloWorldWhenNotAuthenticated(REST_VERSION_2, "Goodbye Anonymous World");
        assertGetAnonymousHelloWorldWhenNotAuthenticated(LATEST, "Goodbye Anonymous World");
    }

    @Test
    public void testGetAnonymousHelloWorldFromSelf() {
        assertEquals("Hello Anonymous World", WebResourceFactory.anonymous(REST_VERSION).path("helloworld").path("callingself").get(String.class));
    }

    private void assertGetAnonymousHelloWorldWhenNotAuthenticated(final String version, final String expected) {
        assertEquals(expected, WebResourceFactory.anonymous(version).path("helloworld").path("anonymous").get(String.class));
    }

    @Test
    public void testGetAnonymousHelloWorldWhenAuthenticated() {
        assertGetAnonymousHelloWorldWhenAuthenticated(REST_VERSION, "Hello Anonymous World");
        assertGetAnonymousHelloWorldWhenAuthenticated(REST_VERSION_2, "Goodbye Anonymous World");
        assertGetAnonymousHelloWorldWhenAuthenticated(LATEST, "Goodbye Anonymous World");
    }

    private void assertGetAnonymousHelloWorldWhenAuthenticated(final String version, final String expected) {
        assertEquals(expected, WebResourceFactory.authenticated(version).path("helloworld").path("anonymous").get(String.class));
    }

    @Test
    public void testGetAuthenticatedHelloWorldWhenAuthenticated() {
        assertGetAuthenticatedHelloWorldWhenAuthenticated(REST_VERSION, "Hello Authenticated World");
        assertGetAuthenticatedHelloWorldWhenAuthenticated(REST_VERSION_2, "Goodbye Authenticated World");
        assertGetAuthenticatedHelloWorldWhenAuthenticated(LATEST, "Goodbye Authenticated World");
    }

    private void assertGetAuthenticatedHelloWorldWhenAuthenticated(final String restVersion, final String expected) {
        assertEquals(expected, WebResourceFactory.authenticated(restVersion).path("helloworld").path("authenticated").get(String.class));
    }

    @Test
    public void testGetAuthenticatedHelloWorldWhenNotAuthenticated() {
        assertGetAuthenticatedHelloWorldWhenNotAuthenticated(REST_VERSION);
        assertGetAuthenticatedHelloWorldWhenNotAuthenticated(REST_VERSION_2);
        assertGetAuthenticatedHelloWorldWhenNotAuthenticated(LATEST);
    }

    private void assertGetAuthenticatedHelloWorldWhenNotAuthenticated(final String version) {
        try {
            WebResourceFactory.anonymous(version).path("helloworld").path("authenticated").get(String.class);
        } catch (UniformInterfaceException e) {
            assertEquals(UNAUTHORIZED.getStatusCode(), e.getResponse().getStatusInfo().getStatusCode());
        }
    }

    @Test
    public void testGetAdminHelloWorldWhenAdmin() {
        assertGetAdminHelloWorldWhenAdmin(REST_VERSION, "Hello admin");
        assertGetAdminHelloWorldWhenAdmin(REST_VERSION_2, "Goodbye admin");
        assertGetAdminHelloWorldWhenAdmin(LATEST, "Goodbye admin");
    }

    private void assertGetAdminHelloWorldWhenAdmin(final String restVersion, final String expected) {
        assertEquals(expected, WebResourceFactory.authenticated(restVersion).path("helloworld").path("admin").get(String.class));
    }

    @Test
    public void testGetAdminHelloWorldWhenNotFred() {
        assertGetAdminHelloWorldWhenNotFred(REST_VERSION);
        assertGetAdminHelloWorldWhenNotFred(REST_VERSION_2);
        assertGetAdminHelloWorldWhenNotFred(LATEST);
    }

    private void assertGetAdminHelloWorldWhenNotFred(final String version) {
        try {
            // fred is an existing user
            WebResourceFactory.authenticate("fred", "fred", version).path("helloworld").path("admin").get(String.class);
        } catch (UniformInterfaceException e) {
            assertEquals(UNAUTHORIZED.getStatusCode(), e.getResponse().getStatusInfo().getStatusCode());
        }
    }

    @Test
    public void testGetAdminHelloWorldWhenNotAuthenticated() {
        assertGetAdminHelloWorldWhenNotAuthenticated(REST_VERSION);
        assertGetAdminHelloWorldWhenNotAuthenticated(REST_VERSION_2);
        assertGetAdminHelloWorldWhenNotAuthenticated(LATEST);
    }

    private void assertGetAdminHelloWorldWhenNotAuthenticated(final String version) {
        try {
            WebResourceFactory.authenticate("bla", "bli", version).path("helloworld").path("admin").get(String.class);
        } catch (UniformInterfaceException e) {
            assertEquals(UNAUTHORIZED.getStatusCode(), e.getResponse().getStatusInfo().getStatusCode());
        }
    }
}
