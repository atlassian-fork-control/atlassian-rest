package it.com.atlassian.rest.interceptor;

import com.atlassian.plugins.rest.interceptor.Message;
import com.atlassian.rest.jersey.client.WebResourceFactory;
import com.sun.jersey.api.client.WebResource;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class InterceptedResourceTest {
    private WebResource webResource;

    @Before
    public void setUp() {
        webResource = WebResourceFactory.authenticated();
    }

    @Test
    public void testGetAutowiredResource() {
        final Message message = webResource.path("interceptedResource").get(Message.class);

        assertEquals("Hello World: Bob", message.getMessage());
    }
}
