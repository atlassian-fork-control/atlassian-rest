package com.atlassian.plugins.rest.module.xml;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import javax.ws.rs.core.Context;
import javax.xml.XMLConstants;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLResolver;
import javax.xml.stream.XMLStreamException;

import com.sun.jersey.core.impl.provider.xml.ThreadLocalSingletonContextProvider;
import com.sun.jersey.core.util.FeaturesAndProperties;

public class XMLStreamReaderContextProvider extends ThreadLocalSingletonContextProvider<XMLInputFactory> {

    private static final InputStream EMPTY_INPUT_STREAM = new ByteArrayInputStream(new byte[0]);

    private final boolean disableXmlSecurity;

    public XMLStreamReaderContextProvider(@Context FeaturesAndProperties fps) {
        super(XMLInputFactory.class);
        disableXmlSecurity = fps.getFeature(FeaturesAndProperties.FEATURE_DISABLE_XML_SECURITY);
    }

    @Override
    protected XMLInputFactory getInstance() {
        XMLInputFactory f = XMLInputFactory.newInstance();
        if (!disableXmlSecurity) {
            f.setProperty(XMLInputFactory.SUPPORT_DTD, Boolean.FALSE);
            f.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES, Boolean.FALSE);
            f.setXMLResolver(new XMLResolver() {
                @Override
                public Object resolveEntity(String publicID, String systemID, String baseURI, String namespace) throws XMLStreamException {
                    // Disable dtd validation
                    return EMPTY_INPUT_STREAM;
                }
            });
        }
        return f;
    }
}