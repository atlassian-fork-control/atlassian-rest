package com.atlassian.plugins.rest.module;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

import static java.util.Arrays.stream;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptySet;
import static java.util.Collections.unmodifiableList;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;

class OsgiServiceAccessor<S> {
    private static final String FILTER = "(|(plugin=com.atlassian.plugins.rest)(" + Constants.BUNDLE_SYMBOLICNAME + "=%s))";

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final Class<S> serviceType;
    private final BundleContext bundleContext;

    private ServiceReference[] references;
    private final OsgiFactory<? extends S> factory;

    OsgiServiceAccessor(final Class<S> serviceType, final BundleContext bundleContext, final OsgiFactory<? extends S> factory) {
        this.serviceType = requireNonNull(serviceType,  "serviceType can't be null");
        this.bundleContext = requireNonNull(bundleContext, "bundleContext can't be null");
        this.factory = requireNonNull(factory, "factory can't be null");
    }

    Collection<? extends S> get() {
        try {
            references = bundleContext.getServiceReferences(serviceType.getName(), createFilterString(bundleContext.getBundle()));
            if (references == null) // no service found
            {
                return emptyList();
            } else {
                return unmodifiableList(stream(references).map(s -> factory.getInstance(bundleContext, s)).collect(toList()));
            }
        } catch (InvalidSyntaxException e) {
            logger.error("Could not get service references", e);
            return emptyList();
        }
    }

    void release() {
        if (references != null) {
            for (ServiceReference reference : references) {
                bundleContext.ungetService(reference);
            }
        }
    }

    private String createFilterString(Bundle currentBundle) {
        return String.format(FILTER, currentBundle.getSymbolicName());
    }
}
