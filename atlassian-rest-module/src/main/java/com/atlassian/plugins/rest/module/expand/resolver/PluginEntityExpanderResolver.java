package com.atlassian.plugins.rest.module.expand.resolver;

import com.atlassian.plugin.module.ContainerManagedPlugin;
import com.atlassian.plugins.rest.common.expand.EntityExpander;
import com.atlassian.plugins.rest.common.expand.Expander;
import com.atlassian.plugins.rest.common.expand.resolver.AbstractAnnotationEntityExpanderResolver;

import static java.util.Objects.requireNonNull;

public class PluginEntityExpanderResolver extends AbstractAnnotationEntityExpanderResolver {
    private final ContainerManagedPlugin plugin;

    public PluginEntityExpanderResolver(ContainerManagedPlugin plugin) {
        this.plugin = requireNonNull(plugin);
    }

    protected final EntityExpander<?> getEntityExpander(Expander expander) {
        return plugin.getContainerAccessor().createBean(expander.value());
    }
}
