package com.atlassian.plugins.rest.module.jersey;

import com.atlassian.plugin.Plugin;
import com.atlassian.sal.api.net.Request;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

@RunWith(value = MockitoJUnitRunner.class)
public class JerseyRequestTest {

    @Mock
    Request delegateRequest;
    @Mock
    Plugin plugin;
    @Mock
    JerseyEntityHandler jerseyEntityHandler;

    /**
     * @throws Exception
     * see REST-262
     */
    @Test
    public void testContentEncodingIsEmpty() throws Exception {
        JerseyRequest jerseyRequest = new JerseyRequest(delegateRequest, jerseyEntityHandler, plugin);
        jerseyRequest.setEntity(new Object());
        jerseyRequest.execute();
        verify(delegateRequest, never()).setHeader(eq("Content-Encoding"), anyString());
    }
}
