package com.atlassian.plugins.rest.doclet.generators.schema;

import com.atlassian.plugins.rest.doclet.generators.schema.beans.artificial.ArrayBean;
import com.atlassian.plugins.rest.doclet.generators.schema.beans.artificial.BeanWithObjectNode;
import com.atlassian.plugins.rest.doclet.generators.schema.beans.artificial.DateTimeBean;
import com.atlassian.plugins.rest.doclet.generators.schema.beans.artificial.DefinitionBean;
import com.atlassian.plugins.rest.doclet.generators.schema.beans.artificial.DescriptionAndRequiredBean;
import com.atlassian.plugins.rest.doclet.generators.schema.beans.artificial.InterfaceBean;
import com.atlassian.plugins.rest.doclet.generators.schema.beans.artificial.NestedGenericType;
import com.atlassian.plugins.rest.doclet.generators.schema.beans.artificial.RawBean;
import com.atlassian.plugins.rest.doclet.generators.schema.beans.artificial.SimpleBean;
import com.atlassian.plugins.rest.doclet.generators.schema.beans.artificial.SimpleListBean;
import com.atlassian.plugins.rest.doclet.generators.schema.beans.artificial.StaticFieldsBean;
import com.atlassian.plugins.rest.doclet.generators.schema.beans.artificial.SubclassedGeneric;
import com.atlassian.plugins.rest.doclet.generators.schema.beans.artificial.TestAbstractBean;
import com.atlassian.plugins.rest.doclet.generators.schema.beans.artificial.UriBean;
import com.atlassian.plugins.rest.doclet.generators.schema.beans.attachment.AttachmentBean;
import com.atlassian.plugins.rest.doclet.generators.schema.beans.attachment.AttachmentViewJsonDto;
import com.atlassian.plugins.rest.doclet.generators.schema.beans.issue.IssueBean;
import com.atlassian.plugins.rest.doclet.generators.schema.beans.jira.PageBean;
import com.atlassian.plugins.rest.doclet.generators.schema.beans.permissions.PermissionsJsonBean;
import com.atlassian.plugins.rest.doclet.generators.schema.beans.permissionscheme.PermissionSchemeBean;
import com.atlassian.rest.annotation.RestProperty;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import static com.atlassian.plugins.rest.doclet.generators.resourcedoc.JsonOperations.toJson;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class SchemaGeneratorTest {
    public static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    @Test
    public void testRequestScopes() {
        testCase(PermissionSchemeBean.class, RestProperty.Scope.REQUEST, "permission-scheme-request.json");
        testCase(PermissionSchemeBean.class, RestProperty.Scope.RESPONSE, "permission-scheme-response.json");
    }

    @Test
    public void testSuperclassFieldsEnumsAndPatternProperties() {
        testCase(PermissionsJsonBean.class, RestProperty.Scope.REQUEST, "permissions.json");
    }

    @Test
    public void testAdvancedGenericTypeResolution() {
        testCase(AttachmentBean.class, RestProperty.Scope.RESPONSE, "attachment.json");
    }

    @Test
    public void testReadableInstantDoesNotThrowNPE() {
        testCase(AttachmentViewJsonDto.class, RestProperty.Scope.RESPONSE, "attachment-view.json");
    }

    @Test
    public void testSimpleGenericList() {
        testCase(SimpleListBean.class, RestProperty.Scope.RESPONSE, "simple-list.json");
    }

    @Test
    public void testNestedGenericTypes() {
        testCase(NestedGenericType.class, RestProperty.Scope.RESPONSE, "nested-generic-type.json");
    }

    @Test
    public void staticFieldsShouldBeIgnored() {
        testCase(StaticFieldsBean.class, RestProperty.Scope.REQUEST, "static-fields.json");
    }

    @Test
    public void fieldsFromInterfacesAreGathered() {
        testCase(InterfaceBean.class, RestProperty.Scope.REQUEST, "interface.json");
    }

    @Test
    public void datesHaveStringFormat() {
        testCase(DateTimeBean.class, RestProperty.Scope.REQUEST, "date-time.json");
    }

    @Test
    public void reusedBeansArePutInDefinitions() {
        testCase(DefinitionBean.class, RestProperty.Scope.REQUEST, "definition.json");
    }

    @Test
    public void testWrappingInList() {
        testCase(List.class, RestProperty.Scope.REQUEST, "simple-wrapped-in-list.json", SimpleBean.class);
    }

    @Test
    public void testWrappingInMap() {
        testCase(Map.class, RestProperty.Scope.REQUEST, "simple-wrapped-in-map.json", String.class, SimpleBean.class);
    }

    @Test
    public void testSubclassedGeneric() {
        testCase(SubclassedGeneric.class, RestProperty.Scope.REQUEST, "subclassed-generic.json");
    }

    @Test
    public void ultimateTest() {
        testCase(IssueBean.class, RestProperty.Scope.RESPONSE, "issue.json");
    }

    @Test
    public void testStringWrappedInList() {
        testCase(List.class, RestProperty.Scope.REQUEST, "list-of-strings.json", String.class);
    }

    @Test
    public void testArrays() {
        testCase(ArrayBean.class, RestProperty.Scope.REQUEST, "array.json");
    }

    @Test
    public void testWrappingInPageBean() {
        testCase(PageBean.class, RestProperty.Scope.REQUEST, "paged-simple.json", SimpleBean.class);
    }

    @Test
    public void testAbstractClasses() {
        testCase(TestAbstractBean.class, RestProperty.Scope.REQUEST, "test-abstract.json");
    }

    @Test
    public void testObjectNodeBean() {
        testCase(BeanWithObjectNode.class, RestProperty.Scope.REQUEST, "object-node.json");
    }

    @Test
    public void testJsonRawValue() {
        testCase(RawBean.class, RestProperty.Scope.REQUEST, "raw.json");
    }

    @Test
    public void uriHasProperFormatSet() {
        testCase(UriBean.class, RestProperty.Scope.REQUEST, "uri.json");
    }

    @Test
    public void testDescriptionAndRequired() {
        testCase(DescriptionAndRequiredBean.class, RestProperty.Scope.REQUEST, "description_and_required.json");
    }

    private void testCase(Class<?> bean, RestProperty.Scope scope, String fileWithExpectedValue, Class<?>... genericTypes) {
        RichClass model = RichClass.of(bean, genericTypes);
        Schema schema = SchemaGenerator.generateSchema(model, scope);

        String actual = formatJson(toJson(schema));
        String expected = getJsonFromFile(fileWithExpectedValue);

        assertThat(actual, equalTo(expected));
    }

    private String getJsonFromFile(final String fileWithExpectedValue) {
        try {
            JsonNode jsonNode = OBJECT_MAPPER.readTree(getClass().getClassLoader().getResourceAsStream(fileWithExpectedValue));
            return jsonNode.toString();
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }

    private String formatJson(String json) {
        try {
            return OBJECT_MAPPER.readTree(json).toString();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
