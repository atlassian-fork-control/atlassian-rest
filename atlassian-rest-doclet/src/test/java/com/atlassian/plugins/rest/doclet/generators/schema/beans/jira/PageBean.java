package com.atlassian.plugins.rest.doclet.generators.schema.beans.jira;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

public class PageBean<T>
{
    @JsonProperty
    private final int maxResults;
    @JsonProperty
    private final long startAt;
    @JsonProperty
    private final long total;
    @JsonProperty
    private final List<T> values;

    public PageBean(final List<T> values, final long total, final int maxResults, final long startAt)
    {
        this.values = values;
        this.total = total;
        this.maxResults = maxResults;
        this.startAt = startAt;
    }

    public long getStartAt()
    {
        return startAt;
    }

    public int getMaxResults()
    {
        return maxResults;
    }

    public long getTotal()
    {
        return total;
    }

    public List<T> getValues()
    {
        return values;
    }
}
