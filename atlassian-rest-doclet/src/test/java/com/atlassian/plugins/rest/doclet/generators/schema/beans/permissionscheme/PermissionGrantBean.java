package com.atlassian.plugins.rest.doclet.generators.schema.beans.permissionscheme;

import com.atlassian.rest.annotation.RestProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;

import java.net.URI;

public final class PermissionGrantBean
{
    @JsonProperty(required = true)
    @RestProperty (scope = RestProperty.Scope.RESPONSE)
    private Long id;
    @JsonProperty(required = true)
    @RestProperty (scope = RestProperty.Scope.RESPONSE)
    private URI self;
    @JsonProperty(required = true)
    @JsonPropertyDescription("a group of people to which the permission is granted")
    private PermissionHolderBean holder;
    @JsonProperty(required = true)
    @JsonPropertyDescription("granted permission")
    private String permission;
}
