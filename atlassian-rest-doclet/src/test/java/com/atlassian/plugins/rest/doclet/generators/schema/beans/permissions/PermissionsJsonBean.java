package com.atlassian.plugins.rest.doclet.generators.schema.beans.permissions;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.Map;

/**
 * Represents a list of Permissions
 *
 * @since v5.0
 */
public class PermissionsJsonBean
{
    @JsonProperty
    private Map<String, ? extends PermissionJsonBean> permissions;
}
