package com.atlassian.plugins.rest.doclet.generators.schema.beans.issue;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.Map;

/**
 * @since v5.0
 */
public class UserJsonBean
{
    @JsonProperty
    private String self;

    @JsonProperty
    private String name;

    @JsonProperty
    private String key;

    @JsonProperty
    private String emailAddress;

    @JsonProperty
    private Map<String, String> avatarUrls;

    @JsonProperty
    private String displayName;

    @JsonProperty
    private boolean active;

    @JsonProperty
    private String timeZone;
}

