package com.atlassian.plugins.rest.doclet.generators.schema.beans.permissions;

import org.codehaus.jackson.annotate.JsonProperty;

public class PermissionJsonBean
{
    @JsonProperty
    public String id;

    @JsonProperty
    public String key;

    @JsonProperty
    public String name;

    @JsonProperty
    public PermissionType type;

    @JsonProperty
    public String description;

    /**
     * Indicate whether this is a project or global permission
     */
    static enum PermissionType
    {
        GLOBAL, PROJECT
    }
}
