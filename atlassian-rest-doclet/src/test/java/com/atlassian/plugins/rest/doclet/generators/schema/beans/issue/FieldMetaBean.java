package com.atlassian.plugins.rest.doclet.generators.schema.beans.issue;

import java.util.Collection;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Represents the meta data of a field.
 *
 * @since v4.2
 */
@XmlRootElement (name = "availableField")
public class FieldMetaBean
{
    @XmlElement
    private boolean required;

    @XmlElement
    private JsonTypeBean schema;

    @XmlElement
    private String name;

    @XmlElement
    private String autoCompleteUrl;

    @XmlElement
    private Boolean hasDefaultValue;

    @XmlElement
    private Collection<String> operations;

    @XmlElement
    private Collection<?> allowedValues;
}
