package com.atlassian.plugins.rest.doclet.generators.schema.beans.permissionscheme;

import com.atlassian.rest.annotation.RestProperty;
import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;
import org.codehaus.jackson.annotate.JsonProperty;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

public final class PermissionSchemeBean
{
    @JsonProperty
    @RestProperty(scope = RestProperty.Scope.RESPONSE)
    private String expand = "permissions";

    @JsonProperty
    @RestProperty(scope = RestProperty.Scope.RESPONSE)
    private Long id;
    @JsonProperty
    @RestProperty(scope = RestProperty.Scope.RESPONSE)
    private URI self;
    @JsonProperty
    private String name;
    @JsonProperty
    private String description;
    @JsonProperty
    private List<PermissionGrantBean> permissions;

    @Deprecated
    public PermissionSchemeBean() {}

    private PermissionSchemeBean(final Long id, final URI self, final String name, final String description, final Iterable<PermissionGrantBean> permissions)
    {
        this.id = id;
        this.self = self;
        this.name = name;
        this.description = description;
        this.permissions = permissions != null ? ImmutableList.copyOf(permissions) : null;
    }

    /**
     * Clears the expand field as it's not expected in the input
     * @return this
     */
    public PermissionSchemeBean input() {
        this.expand = null;
        return this;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public URI getSelf()
    {
        return self;
    }

    public void setSelf(final URI self)
    {
        this.self = self;
    }

    public String getName()
    {
        return name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }

    public List<PermissionGrantBean> getPermissions()
    {
        return permissions;
    }

    public void setPermissions(final ArrayList<PermissionGrantBean> permissions)
    {
        this.permissions = permissions;
    }

    public String getExpand()
    {
        return expand;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }

        PermissionSchemeBean that = (PermissionSchemeBean) o;

        return Objects.equal(this.id, that.id) &&
                Objects.equal(this.self, that.self) &&
                Objects.equal(this.name, that.name) &&
                Objects.equal(this.description, that.description) &&
                Objects.equal(this.permissions, that.permissions);
    }

    @Override
    public int hashCode()
    {
        return Objects.hashCode(id, self, name, description, permissions);
    }

    public static Builder builder()
    {
        return new Builder();
    }

    public static class Builder
    {
        private Long id;
        private URI self;
        private String name;
        private String description;
        private Iterable<PermissionGrantBean> permissions;

        private Builder() {}

        public Builder setId(final Long id)
        {
            this.id = id;
            return this;
        }

        public Builder setSelf(final URI self)
        {
            this.self = self;
            return this;
        }

        public Builder setName(final String name)
        {
            this.name = name;
            return this;
        }

        public Builder setDescription(final String description)
        {
            this.description = description;
            return this;
        }

        public Builder setPermissions(final Iterable<PermissionGrantBean> permissions)
        {
            this.permissions = permissions;
            return this;
        }

        public PermissionSchemeBean build()
        {
            return new PermissionSchemeBean(id, self, name, description, permissions);
        }
    }
}
