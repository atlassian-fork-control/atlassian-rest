package com.atlassian.plugins.rest.doclet.generators.schema.beans.artificial;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonRawValue;

public class RawBean
{
    @JsonProperty
    @JsonRawValue
    String arbitraryJson;
}
