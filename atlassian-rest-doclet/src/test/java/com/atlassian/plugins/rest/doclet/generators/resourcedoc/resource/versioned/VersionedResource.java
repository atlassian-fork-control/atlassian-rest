package com.atlassian.plugins.rest.doclet.generators.resourcedoc.resource.versioned;

import com.sun.jersey.api.model.AbstractResource;

import javax.ws.rs.POST;
/**
 * This class is used in {@link AtlassianWadlGeneratorResourceDocSupportTest}.
 * {@link VersionedResource} is passed to the Constructor of {@link AbstractResource}, where its package information is used to find
 * an associated rest module, in atlassian-plugin.xml.
 * The plugin xml, defines the path and version for the rest API of this resource as :
 *  <rest key="versioned-resource" path="/api" version="2">
 *         <package>com.atlassian.plugins.rest.doclet.generators.resourcedoc.resource.versioned</package>
 *  </rest>
 */
public class VersionedResource {
    @POST
    public void postMethod() {
    }
}
