package com.atlassian.plugins.rest.doclet.generators.schema;

import com.atlassian.rest.annotation.RestProperty.Scope;

import java.util.List;
import java.util.Set;

public class SchemaGenerator {
    private final Scope scope;
    private final Set<ModelClass> schemasWithDefinitions;
    private final ModelClass topLevelModel;

    private SchemaGenerator(final Scope scope, final RichClass topLevelClass) {
        this.scope = scope;
        this.topLevelModel = new ModelClass(topLevelClass, null);
        this.schemasWithDefinitions = topLevelModel.getSchemasReferencedMoreThanOnce(scope);
    }

    public static Schema generateSchema(RichClass modelClass, Scope scope) {
        return new SchemaGenerator(scope, modelClass).generateTopLevel();
    }

    private Schema generateTopLevel() {
        Schema.Builder topLevelSchema = generate(topLevelModel);

        String title = topLevelModel.getTopLevelTitle();
        if (title != null) {
            topLevelSchema.setId("https://docs.atlassian.com/jira/REST/schema/" + Schema.titleToId(title) + "#");
            topLevelSchema.setTitle(title);
        }

        for (ModelClass schemaThatNeedsDefinition : schemasWithDefinitions) {
            topLevelSchema.addDefinition(generate(schemaThatNeedsDefinition).build());
        }

        return topLevelSchema.build();
    }

    private Schema generateWithReferencing(ModelClass model) {
        if (schemasWithDefinitions.contains(model)) {
            return Schema.ref(model.getTitle());
        } else {
            return generate(model).build();
        }
    }

    private Schema.Builder generate(final ModelClass model) {
        final Schema.Builder builder = Schema.builder();

        builder.setType(model.getType());
        builder.setTitle(model.getTitle());
        builder.setDescription(model.getDescription());

        if (model.isAbstract()) {
            List<ModelClass> subModels = model.getSubModels();
            if (subModels.size() > 0) {
                for (ModelClass subModel : subModels) // get subclasses
                {
                    builder.addAnyOf(generateWithReferencing(subModel));
                }
                return builder;
            }
        }

        model.getPatternedProperties().ifPresent(patternAndModel ->
                builder.addPatternProperty(patternAndModel.getPattern(), generateWithReferencing(patternAndModel.getValuesType())));

        model.getCollectionItemModel().ifPresent(modelClass ->
                builder.setItems(generateWithReferencing(modelClass)));

        for (Property property : model.getProperties(scope)) {
            builder.addProperty(property.name, generateWithReferencing(property.model));
            if (property.required) {
                builder.addRequired(property.name);
            }
        }

        if (model.getActualClass().isEnum()) {
            builder.setEnum((Class<Enum>) model.getActualClass());
        }

        return builder;
    }
}
