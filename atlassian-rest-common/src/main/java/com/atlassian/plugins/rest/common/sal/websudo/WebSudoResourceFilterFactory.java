package com.atlassian.plugins.rest.common.sal.websudo;

import com.sun.jersey.api.model.AbstractMethod;
import com.sun.jersey.spi.container.ResourceFilter;
import com.sun.jersey.spi.container.ResourceFilterFactory;

import javax.ws.rs.ext.Provider;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static java.util.Objects.requireNonNull;

/**
 * A {@link com.sun.jersey.spi.container.ResourceFilterFactory} that checks whether the client is authenticated or not.
 *
 * @see WebSudoResourceFilter
 */
@Provider
public class WebSudoResourceFilterFactory implements ResourceFilterFactory {
    private final WebSudoResourceContext authenticationContext;

    public WebSudoResourceFilterFactory(final WebSudoResourceContext authenticationContext) {
        this.authenticationContext = requireNonNull(authenticationContext);
    }

    public List<ResourceFilter> create(final AbstractMethod abstractMethod) {
        return Collections.<ResourceFilter>singletonList(new WebSudoResourceFilter(abstractMethod, authenticationContext));
    }
}