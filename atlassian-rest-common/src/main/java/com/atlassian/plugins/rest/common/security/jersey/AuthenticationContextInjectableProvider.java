package com.atlassian.plugins.rest.common.security.jersey;

import com.atlassian.plugins.rest.common.security.AuthenticationContext;
import com.sun.jersey.spi.inject.SingletonTypeInjectableProvider;

import javax.ws.rs.core.Context;
import javax.ws.rs.ext.Provider;

import static java.util.Objects.requireNonNull;

@Provider
public class AuthenticationContextInjectableProvider extends SingletonTypeInjectableProvider<Context, AuthenticationContext> {
    public AuthenticationContextInjectableProvider(AuthenticationContext authenticationContext) {
        super(AuthenticationContext.class, requireNonNull(authenticationContext));
    }
}
