package com.atlassian.plugins.rest.common.feature.jersey;

import com.atlassian.plugins.rest.common.feature.RequiresDarkFeature;
import com.atlassian.sal.api.features.DarkFeatureManager;
import com.sun.jersey.api.NotFoundException;
import com.sun.jersey.api.model.AbstractMethod;
import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerRequestFilter;
import com.sun.jersey.spi.container.ContainerResponseFilter;
import com.sun.jersey.spi.container.ResourceFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.AnnotatedElement;
import javax.annotation.Nonnull;

import static com.atlassian.plugins.rest.common.util.ReflectionUtils.getAnnotation;
import static java.util.Objects.requireNonNull;

/**
 * Restricts access to resources based on the state of dark feature flags.
 * <p>
 * Used with the {@link RequiresDarkFeature} annotation to control access to resources and methods based on the
 * named dark feature keys.
 *
 * @see RequiresDarkFeature
 */
public class DarkFeatureResourceFilter implements ResourceFilter, ContainerRequestFilter {
    private static final Logger log = LoggerFactory.getLogger(DarkFeatureResourceFilter.class);

    private final DarkFeatureManager darkFeatureManager;
    private final AbstractMethod abstractMethod;

    public DarkFeatureResourceFilter(@Nonnull final AbstractMethod method, @Nonnull final DarkFeatureManager darkFeatureManager) {
        this.darkFeatureManager = requireNonNull(darkFeatureManager, "darkFeatureManager can't be null");
        this.abstractMethod = requireNonNull(method, "method can't be null");
    }

    @Override
    public ContainerRequestFilter getRequestFilter() {
        return this;
    }

    @Override
    public ContainerResponseFilter getResponseFilter() {
        return null;
    }

    @Override
    public ContainerRequest filter(final ContainerRequest request) {
        log.debug("Applying dark feature filter to request {} {}", request.getMethod(), request.getRequestUri());
        if (accessIsAllowed(abstractMethod) && accessIsAllowed(abstractMethod.getResource())) {
            log.debug("Dark feature check OK");
            return request;
        }
        log.debug("Dark feature check failed. Refusing access to the resource.");

        throw new NotFoundException(request.getRequestUri());
    }

    private boolean accessIsAllowed(AnnotatedElement e) {
        if (e == null) {
            return true;
        }

        final RequiresDarkFeature annotation = getAnnotation(RequiresDarkFeature.class, e);
        return annotation == null || allFeaturesAreEnabled(annotation.value());
    }

    private boolean allFeaturesAreEnabled(String[] featureKeys) {
        for (String featureKey : featureKeys) {
            if (!darkFeatureManager.isFeatureEnabledForCurrentUser(featureKey)) {
                return false;
            }
        }
        return true;
    }
}
