package com.atlassian.plugins.rest.common.security.jersey;

import com.atlassian.http.method.Methods;
import com.atlassian.http.mime.BrowserUtils;
import com.atlassian.http.mime.UserAgentUtil.BrowserFamily;
import com.atlassian.http.mime.UserAgentUtilImpl;
import com.atlassian.http.url.SameOrigin;
import com.atlassian.plugin.tracker.PluginModuleTracker;
import com.atlassian.plugins.rest.common.security.CorsHeaders;
import com.atlassian.plugins.rest.common.security.XsrfCheckFailedException;
import com.atlassian.plugins.rest.common.security.descriptor.CorsDefaults;
import com.atlassian.plugins.rest.common.security.descriptor.CorsDefaultsModuleDescriptor;
import com.atlassian.sal.api.web.context.HttpContext;
import com.atlassian.sal.api.xsrf.XsrfRequestValidator;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Predicate;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.ImmutableSet;
import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerRequestFilter;
import com.sun.jersey.spi.container.ContainerResponseFilter;
import com.sun.jersey.spi.container.ResourceFilter;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Locale;
import java.util.stream.StreamSupport;

/**
 * Rejects requests that do not satisfy XSRF checks.
 *
 * A request is rejected if it requires XSRF protection, but does not include the
 * no-check header or a valid XSRF protection token.
 *
 * Also protects browsers against XSRF attacks where the origin of a request would not
 * otherwise be permitted by the same origin policy or CORS.
 *
 * @since 2.4
 */
public class XsrfResourceFilter implements ResourceFilter, ContainerRequestFilter {
    public static final String TOKEN_HEADER = "X-Atlassian-Token";
    public static final String NO_CHECK = "no-check";

    private static final ImmutableSet<String> XSRFABLE_TYPES = ImmutableSet.of(
            MediaType.APPLICATION_FORM_URLENCODED,
            MediaType.MULTIPART_FORM_DATA,
            MediaType.TEXT_PLAIN
    );
    private static final ImmutableSet<String> BROWSER_EXTENSION_ORIGINS = ImmutableSet.of(
            "chrome-extension",
            "safari-extension"
    );
    private static final Logger log = LoggerFactory.getLogger(XsrfResourceFilter.class);
    private static final Cache<String, Boolean> XSRF_NOT_ENFORCED_RESOURCE_CACHE =
        CacheBuilder.newBuilder().maximumSize(1000).build();

    private HttpContext httpContext;
    private XsrfRequestValidator xsrfRequestValidator;
    private PluginModuleTracker<CorsDefaults, CorsDefaultsModuleDescriptor> pluginModuleTracker;
    private Response.Status failureStatus = Response.Status.FORBIDDEN;

    public void setHttpContext(final HttpContext httpContext) {
        this.httpContext = httpContext;
    }

    public void setXsrfRequestValidator(final XsrfRequestValidator xsrfRequestValidator) {
        this.xsrfRequestValidator = xsrfRequestValidator;
    }

    public void setPluginModuleTracker(final PluginModuleTracker<CorsDefaults, CorsDefaultsModuleDescriptor> pluginModuleTracker) {
        this.pluginModuleTracker = pluginModuleTracker;
    }

    public void setFailureStatus(final Response.Status failureStatus) {
        if (!(failureStatus == Response.Status.FORBIDDEN ||
                (failureStatus == Response.Status.NOT_FOUND))) {
            throw new IllegalArgumentException(
                    "Only FORBIDDEN and NOT_FOUND status are valid arguments.");
        }
        this.failureStatus = failureStatus;
    }

    public ContainerRequest filter(final ContainerRequest request) {
        if (passesAllXsrfChecks(request)) {
            return request;
        }

        throw new XsrfCheckFailedException(failureStatus);
    }

    private boolean passesAllXsrfChecks(final ContainerRequest request) {
        final HttpServletRequest httpRequest = getRequestOrNull(httpContext);
        final String method = httpRequest != null && httpRequest.getMethod() != null ?
            httpRequest.getMethod() : request.getMethod();
        final boolean isMethodMutative = Methods.isMutative(method);
        final boolean isPostRequest = isPostRequest(method);

        if (isMethodMutative && isLikelyToBeFromBrowser(request)) {
            final boolean passesOriginChecks = passesAdditionalBrowserChecks(request);
            if (isPostRequest && !passesOriginChecks) {
                return false;
            }
            if (!isPostRequest) {
                if (!passesOriginChecks) {
                    logXsrfFailureButNotBeingEnforced(request, log);
                }
                return true;
            }
        }
        if (isXsrfable(method, request.getMediaType())) {
            final boolean passes = passesStandardXsrfChecks(
                httpRequest) || hasDeprecatedHeaderValue(request);
            if (passes) {
                return true;
            }
            else if (isMethodMutative && !isPostRequest) {
                logXsrfFailureButNotBeingEnforced(request, log);
                return true;
            }
            log.warn(
                    "XSRF checks failed for request: {} , origin: {} , referrer: {}",
                    StringUtils.substringBefore(request.getRequestUri().toString(), "?"),
                    request.getHeaderValue(CorsHeaders.ORIGIN.value()),
                    getSanitisedReferrer(request)
            );
            return false;
        }

        return true;
    }

    void logXsrfFailureButNotBeingEnforced(ContainerRequest request, Logger logger) {
        final String key = request.getPath();
        if (key != null && XSRF_NOT_ENFORCED_RESOURCE_CACHE.getIfPresent(key) == null) {
            logger.warn(
                "XSRF failure not being enforced for request: {} , origin: {} , referrer: {}, " +
                    "method: {}",
                StringUtils.substringBefore(request.getRequestUri().toString(), "?"),
                request.getHeaderValue(CorsHeaders.ORIGIN.value()),
                getSanitisedReferrer(request),
                request.getMethod()
            );
            XSRF_NOT_ENFORCED_RESOURCE_CACHE.put(key, Boolean.TRUE);
        }
    }

    private boolean passesStandardXsrfChecks(final HttpServletRequest httpServletRequest) {
        if (httpServletRequest == null) {
            return false;
        }
        return xsrfRequestValidator.validateRequestPassesXsrfChecks(httpServletRequest);
    }

    /**
     * Returns true if the provided origin is from a browser extension.
     *
     * @param origin the origin to check.
     * @return true if the provided origin is from a browser extension,
     * otherwise returns false.
     */
    boolean isOriginABrowserExtension(String origin) {
        if (StringUtils.isEmpty(origin)) {
            return false;
        }
        try {
            final URI originUri = new URI(origin);
            return BROWSER_EXTENSION_ORIGINS.contains(originUri.getScheme()) &&
                    !originUri.isOpaque();
        } catch (URISyntaxException e) {
            return false;
        }
    }

    /**
     * Due to bugs in some browsers, it is possible for non-simple cross-domain HTTP requests
     * to be issued *without* the normal CORS preflight request being triggered. This meant
     * that both our CORS restrictions and our XSRF protections could be bypassed.
     */
    @VisibleForTesting
    protected boolean passesAdditionalBrowserChecks(final ContainerRequest request) {
        final String origin = request.getHeaderValue(CorsHeaders.ORIGIN.value());
        final String referrer = getSanitisedReferrer(request);
        final URI uri = request.getRequestUri();

        if (isSameOrigin(referrer, uri)) {
            return true;
        }

        if (isSameOrigin(origin, uri)) {
            return true;
        }
        if (isOriginABrowserExtension(origin)) {
            return true;
        }
        final boolean requestContainsCredentials = containsCredentials(request);
        final boolean requestAllowedViaCors = isAllowedViaCors(origin, requestContainsCredentials);

        if (requestAllowedViaCors) {
            return true;
        }
        if (request.getMethod() != null && isPostRequest(request.getMethod())) {
            log.warn("Additional XSRF checks failed for request: {} , " +
                "origin: {} , referrer: {} , credentials in request: {} , " +
                "allowed via CORS: {}",
                StringUtils.substringBefore(uri.toString(), "?"),
                origin,
                referrer,
                requestContainsCredentials,
                requestAllowedViaCors);

        }
        return false;
    }

    boolean isXsrfable(final String method, MediaType mediaType) {
        return method.equals("GET") || (
            Methods.isMutative(method) && (mediaType == null ||
                XSRFABLE_TYPES.contains(mediaTypeToString(mediaType))));
    }

    private boolean hasDeprecatedHeaderValue(final ContainerRequest request) {
        final String tokenHeader = request.getHeaderValue(TOKEN_HEADER);

        if (tokenHeader == null) {
            return false;
        }

        final String normalisedTokenHeader = tokenHeader.toLowerCase(Locale.ENGLISH);

        if (normalisedTokenHeader.equals("nocheck")) {
            log.warn("Use of the 'nocheck' value for {} " +
                    "has been deprecated since rest 3.0.0. Please use a value of " +
                    "'no-check' instead.", TOKEN_HEADER);
            return true;
        }

        return false;
    }

    private boolean isSameOrigin(final String uri, final URI origin) {
        try {
            return StringUtils.isNotEmpty(uri) && SameOrigin.isSameOrigin(new URI(uri), origin);
        } catch (MalformedURLException e) {
            return false;
        } catch (URISyntaxException e) {
            return false;
        } catch (IllegalArgumentException e) {
            return false;
        }
    }

    private boolean isAllowedViaCors(final String originUri, final boolean withCredentials) {
        if (originUri == null) {
            return false;
        }

        return StreamSupport.stream(pluginModuleTracker.getModules().spliterator(), false).anyMatch(new Predicate<CorsDefaults>() {
            public boolean apply(CorsDefaults delegate) {
                if (!delegate.allowsOrigin(originUri)) {
                    return false;
                } else if (withCredentials && !delegate.allowsCredentials(originUri)) {
                    return false;
                }

                return true;
            }
        });
    }

    public ContainerRequestFilter getRequestFilter() {
        return this;
    }

    public ContainerResponseFilter getResponseFilter() {
        return null;
    }

    private static boolean containsCredentials(final ContainerRequest request) {
        return containsCookies(request) || containsHttpAuthHeader(request);
    }

    private static boolean containsCookies(final ContainerRequest request) {
        return !request.getCookies().isEmpty();
    }

    private static boolean containsHttpAuthHeader(final ContainerRequest request) {
        return StringUtils.isNotEmpty(request.getHeaderValue("Authorization"));
    }

    static boolean isPostRequest(final String method) {
        return method.equals("POST");
    }

    boolean isLikelyToBeFromBrowser(final ContainerRequest request) {
        final String userAgent = request.getHeaderValue("User-Agent");
        final BrowserFamily browserFamily = new UserAgentUtilImpl().getBrowserFamily(userAgent);
        if ((passesStandardXsrfChecks(getRequestOrNull(httpContext)) || hasDeprecatedHeaderValue(request)) && BrowserUtils.isIE(userAgent)) {
            return false;
        }
        return !browserFamily.equals(BrowserFamily.UKNOWN);
    }

    private static HttpServletRequest getRequestOrNull(final HttpContext httpContext) {
        return (httpContext == null) ? null : httpContext.getRequest();
    }

    private static String mediaTypeToString(MediaType mediaType) {
        return mediaType.getType().toLowerCase(Locale.ENGLISH) + "/" + mediaType.getSubtype().toLowerCase(Locale.ENGLISH);
    }

    private static String getSanitisedReferrer(final ContainerRequest request) {
        return StringUtils.substringBefore(request.getHeaderValue("Referer"), "?");
    }

}
