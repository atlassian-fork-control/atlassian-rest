package com.atlassian.plugins.rest.common.multipart;

import com.google.common.base.SuppliersTestUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.RestoreSystemProperties;
import org.junit.rules.TestRule;
import static org.junit.Assert.assertEquals;

public class FileSizeLimitExceededExceptionTest {

    @Rule
    public final TestRule restoreSystemProperties = new RestoreSystemProperties();

    @Before
    public void setUp() {
        SuppliersTestUtils.resetMemoizingSupplier(FileSizeLimitExceededException.legacyMode);
    }

    @Test
    public void isExpectedStatusCode() {
        FileSizeLimitExceededException ex = new FileSizeLimitExceededException("Simple message");
        assertEquals(413, ex.getResponse().getStatus());
    }

    @Test
    public void legacyModeChangesStatusCode() {
        System.setProperty(FileSizeLimitExceededException.LEGACY_MODE_KEY, "true");

        FileSizeLimitExceededException ex = new FileSizeLimitExceededException("Simple message");
        assertEquals(404, ex.getResponse().getStatus());
    }
}
