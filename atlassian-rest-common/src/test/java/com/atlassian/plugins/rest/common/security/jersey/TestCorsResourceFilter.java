package com.atlassian.plugins.rest.common.security.jersey;

import com.atlassian.plugin.tracker.PluginModuleTracker;
import com.atlassian.plugins.rest.common.security.CorsHeaders;
import com.atlassian.plugins.rest.common.security.CorsPreflightCheckCompleteException;
import com.atlassian.plugins.rest.common.security.descriptor.CorsDefaults;
import com.atlassian.plugins.rest.common.security.descriptor.CorsDefaultsModuleDescriptor;
import com.google.common.collect.ImmutableSet;
import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerResponse;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.google.common.collect.Maps.newHashMap;
import static com.google.common.collect.Sets.newHashSet;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.Silent.class)
public class TestCorsResourceFilter {
    @Mock
    private CorsDefaults corsDefaults;
    @Mock
    private CorsDefaults corsDefaults2;
    private CorsResourceFilter corsResourceFilter;
    @Mock
    private ContainerRequest request;
    private Map<String, Object> requestProps;
    @Mock
    private ContainerResponse response;
    @Mock
    private PluginModuleTracker<CorsDefaults, CorsDefaultsModuleDescriptor> tracker;

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Before
    public void setUp() {
        when(tracker.getModules()).thenReturn(newHashSet(corsDefaults));
        when(response.getResponse()).thenReturn(Response.ok().build());
        requestProps = newHashMap();
        when(request.getProperties()).thenReturn(requestProps);
        corsResourceFilter = new CorsResourceFilter(tracker, "GET");
    }

    @Test
    public void testSimplePreflightForGet() {
        String origin = "http://localhost";
        requestProps.put("Cors-Preflight-Requested", "true");
        when(corsDefaults.allowsOrigin(origin)).thenReturn(true);
        when(request.getHeaderValue("Access-Control-Request-Method")).thenReturn("GET");
        when(request.getHeaderValue("Origin")).thenReturn(origin);
        MultivaluedMap<String, Object> headers = execPreflight();
        assertEquals(headers.getFirst("Access-Control-Allow-Origin"), origin);
    }

    @Test
    public void testPreflightSucceedsWhenOneCorsDefaultsAllowsOrigin() {
        String origin = "http://localhost";
        MultivaluedMap<String, Object> headers = execPreflightWithTwoCorsDefaults(origin);
        assertEquals(headers.getFirst("Access-Control-Allow-Origin"), origin);
    }

    @Test
    public void testSecondCorsDefaultsIsNotHitIfDoesntAllowOrigin() {
        String origin = "http://localhost";
        MultivaluedMap<String, Object> headers = execPreflightWithTwoCorsDefaults(origin);
        verify(corsDefaults2, never()).allowsCredentials(any());
        verify(corsDefaults2, never()).getAllowedRequestHeaders(any());
        verify(corsDefaults2, never()).getAllowedResponseHeaders(any());
    }

    @Test
    public void relativeOriginUriIsNotAllowed() {
        MultivaluedMap<String, Object> headers = execPreflightWithTwoCorsDefaults(
                "/not-absolute.com");
        assertNull(headers.get(CorsHeaders.ACCESS_CONTROL_ALLOW_ORIGIN.value()));
    }

    @Test
    public void opaqueOriginUriIsNotAllowed() {
        MultivaluedMap<String, Object> headers = execPreflightWithTwoCorsDefaults(
                "opaque:test.com");
        assertNull(headers.get(CorsHeaders.ACCESS_CONTROL_ALLOW_ORIGIN.value()));
    }

    @Test
    public void nullOriginIsNotAllowed() {
        MultivaluedMap<String, Object> headers = execPreflightWithTwoCorsDefaults(
                "null");
        assertNull(headers.get(CorsHeaders.ACCESS_CONTROL_ALLOW_ORIGIN.value()));
    }

    @Test
    public void testSimplePreflightForGetWrongDomain() {
        String origin = "http://localhost";
        requestProps.put("Cors-Preflight-Requested", "true");
        when(corsDefaults.allowsOrigin(origin)).thenReturn(false);
        when(request.getHeaderValue("Access-Control-Request-Method")).thenReturn("GET");
        when(request.getHeaderValue("Origin")).thenReturn(origin);
        exception.expect(CorsPreflightCheckCompleteException.class);
        execBadPreflight();
    }

    @Test
    public void testSimplePreflightForGetWrongMethod() {
        String origin = "http://localhost";
        requestProps.put("Cors-Preflight-Requested", "true");
        when(corsDefaults.allowsOrigin(origin)).thenReturn(false);
        when(request.getHeaderValue("Access-Control-Request-Method")).thenReturn("POST");
        when(request.getHeaderValue("Origin")).thenReturn(origin);
        exception.expect(CorsPreflightCheckCompleteException.class);
        execBadPreflight();
    }

    @Test
    public void testSimplePreflightForGetWrongHeaders() {
        String origin = "http://localhost";
        requestProps.put("Cors-Preflight-Requested", "true");
        when(corsDefaults.allowsOrigin(origin)).thenReturn(true);
        when(corsDefaults.getAllowedRequestHeaders(origin)).thenReturn(ImmutableSet.of("Foo-Header"));
        when(request.getHeaderValue("Access-Control-Request-Method")).thenReturn("GET");
        when(request.getRequestHeader("Access-Control-Request-Headers")).thenReturn(Collections.singletonList("Bar-Header"));
        when(request.getHeaderValue("Origin")).thenReturn(origin);
        exception.expect(CorsPreflightCheckCompleteException.class);
        execBadPreflight();
    }

    @Test
    public void testSimpleGet() {
        String origin = "http://localhost";
        when(request.getMethod()).thenReturn("GET");
        when(corsDefaults.allowsOrigin(origin)).thenReturn(true);
        when(request.getHeaderValue("Origin")).thenReturn(origin);
        MultivaluedMap<String, Object> headers = execNoPreflightWithHeaders();
        assertEquals(headers.getFirst("Access-Control-Allow-Origin"), origin);
    }

    @Test
    public void testSimpleGetWhenOneCorsDefaultsAllowsOrigin() {
        String origin = "http://localhost";
        MultivaluedMap<String, Object> headers = execNoPreflightWithHeadersForTwoCorsDefaults(origin);
        assertEquals(headers.getFirst("Access-Control-Allow-Origin"), origin);
    }

    @Test
    public void testSecondCorsDefaultIsNotCalledWhenItDoesntAllowOrigin() {
        String origin = "http://localhost";
        execNoPreflightWithHeadersForTwoCorsDefaults(origin);
        verify(corsDefaults2, never()).allowsCredentials(any());
        verify(corsDefaults2, never()).getAllowedRequestHeaders(any());
        verify(corsDefaults2, never()).getAllowedResponseHeaders(any());
    }

    @Test
    public void testSimpleGetWrongOrigin() {
        String origin = "http://localhost";
        when(request.getMethod()).thenReturn("GET");
        when(corsDefaults.allowsOrigin(origin)).thenReturn(true);
        when(request.getHeaderValue("Origin")).thenReturn("http://foo.com");
        execNoPreflightNoHeaders();
    }

    @Test
    public void testSimpleGetNoOrigin() {
        when(request.getMethod()).thenReturn("GET");
        execNoPreflightNoHeaders();
    }

    @Test
    public void testSimplePreflightWithMultipleRequestHeaderValues() {
        execPreflightWithRequestHeaders(Collections.singletonList("foo-header,bar-header"));
        assertEquals("true", requestProps.get("Cors-Preflight-Succeeded"));
    }

    @Test
    public void simplePreflightWithMixedCasingRequestHeaderValues() {
        execPreflightWithRequestHeaders(
                Collections.singletonList("fOO-header,bar-header"),
                ImmutableSet.of("Foo-Header", "Bar-Header")
        );
        assertEquals("true", requestProps.get("Cors-Preflight-Succeeded"));
    }

    @Test
    public void testSimplePreflightWithMultipleRequestHeaders() {
        execPreflightWithRequestHeaders(Arrays.asList("foo-header", "bar-header"));
        assertEquals("true", requestProps.get("Cors-Preflight-Succeeded"));
    }

    @Test
    public void testSimplePreflightWithSpacesInRequestHeaders() {
        execPreflightWithRequestHeaders(Collections.singletonList(" foo-header, bar-header "));
        assertEquals("true", requestProps.get("Cors-Preflight-Succeeded"));
    }

    @Test
    public void preflightWithMultipleRequestHeadersHasExpectedAccessControlAllowHeaders() {
        final String headerName = CorsHeaders.ACCESS_CONTROL_ALLOW_HEADERS.value();
        Set<String> values = ImmutableSet.of("foo-header", "bar-header");
        MultivaluedMap<String, Object> headers = execPreflightWithRequestHeaders(
                Collections.emptyList(), values);
        assertThat(headers.get(headerName).size(), is(1));
        assertThat(ImmutableSet.copyOf(
                headers.getFirst(headerName).toString().split(", ")),
                is(values));
    }

    @Test
    public void corsResponseHasExpectedAccessControlExposeHeaders() {
        final String origin = "http://localhost";
        final String headerName = CorsHeaders.ACCESS_CONTROL_EXPOSE_HEADERS.value();
        Set<String> values = ImmutableSet.of("foo-header", "bar-header");

        when(corsDefaults.getAllowedResponseHeaders(origin)).thenReturn(values);
        when(corsDefaults.allowsOrigin(origin)).thenReturn(true);
        when(request.getHeaderValue("Origin")).thenReturn(origin);

        MultivaluedMap<String, Object> headers = execNoPreflightWithHeaders();
        assertThat(headers.get(headerName).size(), is(1));
        assertThat(ImmutableSet.copyOf(
                headers.getFirst(headerName).toString().split(", ")),
                is(values));
    }

    private MultivaluedMap<String, Object> execPreflight() {
        try {
            corsResourceFilter.filter(request);
            fail("Should have thrown preflight exception");
            return null;
        } catch (CorsPreflightCheckCompleteException ex) {
            return ex.getResponse().getMetadata();
        }
    }

    private MultivaluedMap<String, Object> execNoPreflightWithHeaders() {
        corsResourceFilter.filter(request);
        ArgumentCaptor<Response> argument = ArgumentCaptor.forClass(Response.class);
        corsResourceFilter.filter(request, response);
        verify(response).setResponse(argument.capture());
        return argument.getValue().getMetadata();
    }

    private void execNoPreflightNoHeaders() {
        try {
            corsResourceFilter.filter(request);
            ArgumentCaptor<Response> argument = ArgumentCaptor.forClass(Response.class);
            corsResourceFilter.filter(request, response);
            verify(response, never()).setResponse(argument.capture());
        } catch (CorsPreflightCheckCompleteException ex) {
            fail("Shouldn't have thrown preflight exception");
        }
    }

    private MultivaluedMap<String, Object> execPreflightWithRequestHeaders(
            List<String> headers,
            Set<String> allowedHeaders) {
        String origin = "http://localhost";
        requestProps.put("Cors-Preflight-Requested", "true");
        when(corsDefaults.allowsOrigin(origin)).thenReturn(true);
        when(corsDefaults.getAllowedRequestHeaders(origin)).thenReturn(allowedHeaders);
        when(request.getHeaderValue("Access-Control-Request-Method")).thenReturn("GET");
        when(request.getRequestHeader("Access-Control-Request-Headers")).thenReturn(headers);
        when(request.getHeaderValue("Origin")).thenReturn(origin);
        return execPreflight();
    }

    private MultivaluedMap<String, Object> execPreflightWithRequestHeaders(
            List<String> headers) {
        return execPreflightWithRequestHeaders(headers,
                ImmutableSet.of("foo-header", "bar-header"));
    }

    private void execBadPreflight()
            throws CorsPreflightCheckCompleteException {
        corsResourceFilter.filter(request);
        corsResourceFilter.filter(request, response);
    }

    private MultivaluedMap<String, Object> execPreflightWithTwoCorsDefaults(String origin) {
        requestProps.put("Cors-Preflight-Requested", "true");
        when(tracker.getModules()).thenReturn(newHashSet(corsDefaults, corsDefaults2));
        when(corsDefaults.allowsOrigin(origin)).thenReturn(true);
        when(corsDefaults2.allowsOrigin(origin)).thenReturn(false);
        when(request.getHeaderValue("Access-Control-Request-Method")).thenReturn("GET");
        when(request.getHeaderValue("Origin")).thenReturn(origin);
        return execPreflight();
    }

    private MultivaluedMap<String, Object> execNoPreflightWithHeadersForTwoCorsDefaults(String origin) {
        when(request.getMethod()).thenReturn("GET");
        when(tracker.getModules()).thenReturn(newHashSet(corsDefaults, corsDefaults2));
        when(corsDefaults.allowsOrigin(origin)).thenReturn(true);
        when(corsDefaults2.allowsOrigin(origin)).thenReturn(false);
        when(request.getHeaderValue("Origin")).thenReturn(origin);
        return execNoPreflightWithHeaders();
    }
}
