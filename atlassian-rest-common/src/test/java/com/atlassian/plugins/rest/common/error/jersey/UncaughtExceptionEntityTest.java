package com.atlassian.plugins.rest.common.error.jersey;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;

import com.google.common.collect.ImmutableList;
import com.sun.jersey.core.header.InBoundHeaders;
import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.WebApplication;

import org.junit.Test;

import static org.mockito.Mockito.when;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;

public class UncaughtExceptionEntityTest {
    @Test
    public void stackTraceIsNotNull() {
        assertNotNull(new UncaughtExceptionEntity(new Throwable()).getStackTrace());
    }

    @Test
    public void messagesAreTakenFromThrowables() {
        assertNull(new UncaughtExceptionEntity(new Throwable()).getMessage());
        assertEquals("Message",
                new UncaughtExceptionEntity(new Throwable("Message")).getMessage());
    }

    @Test
    public void defaultConstructorSucceeds() {
        UncaughtExceptionEntity uee = new UncaughtExceptionEntity();
        assertNull(uee.getMessage());
    }

    @Test
    public void defaultIsXmlWithRealContainerRequestImplementation() {
        WebApplication wa = mock(WebApplication.class);
        InBoundHeaders headers = mock(InBoundHeaders.class);

        Request req = new ContainerRequest(wa, null, null, null, headers, null);

        MediaType mt = UncaughtExceptionEntity.variantFor(req);

        assertEquals(MediaType.APPLICATION_XML_TYPE,
                new MediaType(mt.getType(), mt.getSubtype()));
    }

    @Test
    public void textPlainIncludeCharsetParameterWithRealContainerRequestImplementation() {
        WebApplication wa = mock(WebApplication.class);
        InBoundHeaders headers = mock(InBoundHeaders.class);

        when(headers.get(HttpHeaders.ACCEPT)).thenReturn(ImmutableList.of("text/plain"));

        Request req = new ContainerRequest(wa, null, null, null, headers, null);

        MediaType mt = UncaughtExceptionEntity.variantFor(req);

        assertEquals(MediaType.valueOf("text/plain; charset=utf-8"), mt);
    }
}
