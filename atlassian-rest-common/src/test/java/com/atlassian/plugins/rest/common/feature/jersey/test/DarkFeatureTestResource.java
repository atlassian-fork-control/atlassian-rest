package com.atlassian.plugins.rest.common.feature.jersey.test;

import com.atlassian.plugins.rest.common.feature.RequiresDarkFeature;

import java.lang.reflect.Method;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import static java.lang.String.format;

/**
 * A dummy resource that includes annotation and non-annotated methods.
 * <p>
 * For use in unit tests.
 */
@SuppressWarnings("unused")
@Path("/darkfeaturetest")
public class DarkFeatureTestResource {
    public static final String FEATURE_KEY = "feature.key";

    @GET
    @Path("/annotated")
    @RequiresDarkFeature(FEATURE_KEY)
    public Response annotatedMethod() {
        return Response.ok().build();
    }

    @GET
    @Path("/nonAnnotated")
    public Response nonAnnotatedMethod() {
        return Response.ok().build();
    }

    public static Method getAnnotatedMethod() {
        return getMethod("annotatedMethod");
    }

    public static Method getNonAnnotatedMethod() {
        return getMethod("nonAnnotatedMethod");
    }

    private static Method getMethod(final String name) {
        try {
            return DarkFeatureTestResource.class.getMethod(name);
        } catch (NoSuchMethodException e) {
            throw new IllegalStateException(format("Method '%s' not found. Did you accidentally delete it?", name));
        }
    }
}
